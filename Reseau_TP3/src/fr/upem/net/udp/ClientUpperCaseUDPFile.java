package fr.upem.net.udp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientUpperCaseUDPFile {

	private static final Logger logger = Logger.getLogger(ClientUpperCaseUDPFile.class.getName());
	private static final Charset UTF8 = Charset.forName("UTF8");
	private static final int BUFFER_SIZE = 1024;

	private static void usage() {
		System.out.println("Usage : ClientUpperCaseUDPFile in-filename out-filename timeout host port ");
	}

	private static Thread createRespondingThread(DatagramChannel dc, BlockingQueue<ByteBuffer> queue) {
		return new Thread(() -> {
			try {
				ByteBuffer receiveData = ByteBuffer.allocateDirect(BUFFER_SIZE);
				while (!Thread.interrupted()) {
					receiveData.clear();
					dc.receive(receiveData);
					receiveData.flip();
					ByteBuffer copy = ByteBuffer.allocateDirect(receiveData.remaining());
					copy.put(receiveData);
					queue.put(copy);
				}
			} catch (InterruptedException | AsynchronousCloseException e) {
				// normal behaviour
			} catch (IOException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		});
	}

	public static Optional<String> toUpperCase(String msg, DatagramChannel dc, SocketAddress dest, int timeout,
		BlockingQueue<ByteBuffer> queue) {
		try {
			ByteBuffer data = UTF8.encode(msg);
			boolean isResponseReceive = false;
			while (!isResponseReceive) {
					dc.send(data, dest);
					ByteBuffer receiveData = queue.poll(timeout, TimeUnit.MILLISECONDS);
					if (receiveData == null) {
						logger.log(Level.INFO, "Le serveur n'a pas répondu. Renvoie du message: " + msg);
						data.flip();
					} else {
						receiveData.flip();
						isResponseReceive = true;
						String response = Charset.forName("utf8").decode(receiveData).toString();
						return Optional.ofNullable(response);
					}
			}
		} catch (InterruptedException | IOException e) {
			return Optional.empty();
		}
		return Optional.empty();
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length != 5) {
			usage();
			return;
		}

		String inFilename = args[0];
		String outFilename = args[1];
		int timeout = Integer.valueOf(args[2]);
		String host = args[3];
		int port = Integer.valueOf(args[4]);
		SocketAddress dest = new InetSocketAddress(host, port);
		SynchronousQueue<ByteBuffer> queue = new SynchronousQueue<>();

		// Read all lines of inFilename opened in UTF-8
		List<String> lines = Files.readAllLines(Paths.get(inFilename), UTF8);
		ArrayList<String> upperCaseLines = new ArrayList<>();

		try (DatagramChannel dc = DatagramChannel.open()) {
			Thread respondingThread = createRespondingThread(dc, queue);
			respondingThread.start();

			for (String line : lines) {
				toUpperCase(line, dc, dest, timeout, queue).ifPresent(upperCaseLines::add);
				/*
				 * logger.log(Level.INFO, "---------------------------------------- Traitement de la ligne: " + line);
				 * 
				 * ByteBuffer data = UTF8.encode(line); boolean isResponseReceive = false; while (!isResponseReceive) {
				 * try { dc.send(data, dest); ByteBuffer receiveData = queue.poll(timeout, TimeUnit.MILLISECONDS); if
				 * (receiveData == null) { logger.log(Level.INFO, "Le serveur n'a pas répondu. Renvoie du message: " +
				 * line); data.flip(); } else { receiveData.flip(); isResponseReceive = true; String receiveMsg =
				 * Charset.forName("utf8").decode(receiveData).toString(); upperCaseLines.add(receiveMsg); count++;
				 * logger.log(Level.INFO, "Receive: " + receiveMsg); logger.info(upperCaseLines.toString()); } } catch
				 * (InterruptedException e) { logger.log(Level.INFO, e.getMessage()); } catch (IOException e) {
				 * logger.log(Level.SEVERE, e.getMessage()); }
				 */
			}
			respondingThread.interrupt();
		}

		// Write upperCaseLines to outFilename in UTF-8
		Files.write(Paths.get(outFilename), upperCaseLines, UTF8, StandardOpenOption.CREATE, StandardOpenOption.WRITE,
			StandardOpenOption.TRUNCATE_EXISTING);

	}

}
