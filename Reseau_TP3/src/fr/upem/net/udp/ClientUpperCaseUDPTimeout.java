package fr.upem.net.udp;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientUpperCaseUDPTimeout {

	private static final Logger logger = Logger.getLogger(ClientUpperCaseUDPTimeout.class.getName());
	private static final int BUFFER_CAPACITY = 1024;
	private static final int TIMEOUT = 1000;

	private static void usage() {
		System.out.println("address port data");
	}

	private static Thread createRespondingThread(DatagramChannel dc, BlockingQueue<ByteBuffer> queue) {
		return new Thread(() -> {
			try {
				ByteBuffer receiveData = ByteBuffer.allocateDirect(BUFFER_CAPACITY);
				while (!Thread.interrupted()) {
					receiveData.clear();
					dc.receive(receiveData);
					receiveData.flip();
					ByteBuffer copy = ByteBuffer.allocateDirect(receiveData.remaining());
					copy.put(receiveData);
					queue.put(copy);
				}
			} catch (AsynchronousCloseException e) {
				logger.log(Level.INFO, e.getMessage());
			} catch (IOException e) {
				logger.log(Level.SEVERE, e.getMessage());
			} catch (InterruptedException e) {
				logger.log(Level.INFO, e.getMessage());
			}
		});
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			usage();
		}
		String address = args[0];
		int port = Integer.parseInt(args[1]);

		InetAddress inetAddress = Inet4Address.getByName(address);
		InetSocketAddress dest = new InetSocketAddress(inetAddress, port);

		try (DatagramChannel dc = DatagramChannel.open()) {
			SynchronousQueue<ByteBuffer> queue = new SynchronousQueue<>();
			Thread respondingThread = createRespondingThread(dc, queue);
			respondingThread.start();

			try (Scanner scan = new Scanner(System.in)) {
				while (scan.hasNextLine()) {
					String line = scan.nextLine();
					ByteBuffer bb = Charset.forName("utf8").encode(line);
					boolean isResponseReceive = false;

					while (!isResponseReceive) {
						dc.send(bb, dest);
						try {
							ByteBuffer response = queue.poll(TIMEOUT, TimeUnit.MILLISECONDS);
							if (response == null) {
								logger.log(Level.INFO, "Le serveur n'a pas répondu. Renvoie du message: " + line);
								bb.flip();
							} else {
								response.flip();
								isResponseReceive = true;
								logger.log(Level.INFO, "> " + Charset.forName("utf8").decode(response));
							}
						} catch (InterruptedException e) {
							logger.log(Level.INFO, e.getMessage());
						}
					}
				}
				respondingThread.interrupt();
			} 
		}

	}

}
