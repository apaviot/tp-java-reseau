package fr.upem.net.buffers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class StoreWithByteOrder {
	private static final int INITIAL_BUFFER_CAPACITY = 256;
	
	private static ByteBuffer createByteBuffer(String endianess) {
		ByteBuffer bb = ByteBuffer.allocateDirect(INITIAL_BUFFER_CAPACITY);
		switch (endianess) {
		case "BE":
			return bb;
		case "LE" :
			return bb.order(ByteOrder.LITTLE_ENDIAN);
		default :
			throw new IllegalStateException("Illegal endianess: " + endianess);
		}
	}

	public static void main(String[] args) throws IOException {
		String endianess = args[0];
		ByteBuffer bb = createByteBuffer(endianess);
		try (FileChannel fc = FileChannel.open(Paths.get(args[1]), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING)) {
			try (Scanner scan = new Scanner(System.in)) {
				while (scan.hasNextLong()) {
					bb.putLong(scan.nextLong());
					if (bb.remaining() < Long.BYTES) {
						bb.flip();
						fc.write(bb);
						bb.clear();
					}
				}
				bb.flip();
				fc.write(bb);
			}
		}
	}

}
