package fr.upem.net.buffers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class LoadWithEncoding {
	private static final int INITIAL_BUFFER_CAPACITY = 256;

    private static void usage(){
        System.out.println("Usage: LoadWithEncoding charset [filename]");
    }

    private static String stringFromFile(Charset cs, Path path) throws IOException {
    	try (FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)) {
    		ByteBuffer bb = ByteBuffer.allocateDirect((int) fc.size());
    		while (bb.hasRemaining()) {
    			fc.read(bb);
    		}
    		bb.flip();
    		return cs.decode(bb).toString();
    	}
    }

    private static String stringFromStandardInput(Charset cs) throws IOException {
    	try (ReadableByteChannel in = Channels.newChannel(System.in)) {
    		ByteBuffer bb = ByteBuffer.allocateDirect(INITIAL_BUFFER_CAPACITY);
    		while (in.read(bb) != -1) {
    			if (!bb.hasRemaining()) {
    				bb = growBufferCapacity(bb);
    			}
    		}
    		bb.flip();
    		return cs.decode(bb).toString();
    	}
    }
    
    private static ByteBuffer growBufferCapacity(ByteBuffer src) {
    	src.flip();
		ByteBuffer dst = ByteBuffer.allocate(src.capacity() * 2);
		dst.put(src);
		return dst;
    }

    public static void main(String[] args) throws IOException {
        if (args.length!=2 && args.length!=1){
            usage();
            return;
        }
        Charset cs=Charset.forName(args[0]);
        if (args.length==2) {
            Path path = Paths.get(args[1]);
            System.out.println(stringFromFile(cs,path));
        }

        if (args.length==1) {
            System.out.println(stringFromStandardInput(cs));
        }
    }


}