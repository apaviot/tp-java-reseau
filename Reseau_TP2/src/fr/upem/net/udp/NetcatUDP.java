package fr.upem.net.udp;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.util.Scanner;

public class NetcatUDP {
	private static final int BUFFER_CAPACITY = 1024;
	
	private static void usage() {
		System.out.println("address port data");
	}
	
	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			usage();
		}
		String address = args[0];
		int port = Integer.parseInt(args[1]);
	
		InetAddress inetAddress = Inet4Address.getByName(address);
		InetSocketAddress dest = new InetSocketAddress(inetAddress, port);
		
		try (DatagramChannel dc = DatagramChannel.open()) {
			try (Scanner scan = new Scanner(System.in)) {
				ByteBuffer bufferDst = ByteBuffer.allocateDirect(BUFFER_CAPACITY);
				
	    		while (scan.hasNextLine()) {
	    			ByteBuffer bb = Charset.forName("utf8").encode(scan.nextLine());
	    			dc.send(bb, dest);
	    			
	    			InetSocketAddress socket = (InetSocketAddress) dc.receive(bufferDst);
	    			bufferDst.flip();
		    		System.out.println("> " + Charset.forName("utf8").decode(bufferDst));
		    		System.out.println("Received " + bufferDst.limit() + " bytes from " + socket.getHostName());
		    		bufferDst.clear();
	    		}
	    	}
		}
		
		
		
		
	}

}
