package fr.upem.net.udp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

public class ClientBetterUpperCaseUDP {

	public static Charset ASCII_CHARSET = Charset.forName("US-ASCII");
	public static int BUFFER_SIZE = 1024;

	/**
	 * Create and return the String message represented by the ByteBuffer buffer in the following representation: - the
	 * size (as an Big Indian int) of the charsetName encoding in ASCII<br/>
	 * - the bytes encoding this charsetName in ASCII<br/>
	 * - the bytes encoding the message in this charsetName.<br/>
	 * The accepted ByteBuffer buffer must be in <strong>write mode</strong> (need to be flipped before to be used).
	 *
	 * @param buffer
	 *            a ByteBuffer containing the representation of a message
	 * @return the String represented by bb or Optional.empty() if the bb is mal-formed
	 */
	public static Optional<String> decodeMessage(ByteBuffer buffer) {
		Objects.requireNonNull(buffer).flip();

		if (buffer.remaining() < Integer.BYTES) {
			return Optional.empty();
		}

		int charsetNameLength = buffer.getInt();
		if (charsetNameLength <= 0 || charsetNameLength > buffer.limit()) {
			return Optional.empty();
		}

		int currentLimit = buffer.limit();
		buffer.limit(buffer.position() + charsetNameLength);
		String charsetName = ASCII_CHARSET.decode(buffer).toString();
		
		try {
			Charset cs = Charset.forName(charsetName);
			buffer.limit(currentLimit);
			return Optional.ofNullable(cs.decode(buffer).toString());
		} catch (IllegalCharsetNameException | UnsupportedCharsetException e) {
			return Optional.empty();
		}
	}

	/**
	 * Create and return a new ByteBuffer containing the representation of msg string using the charset charsetName in
	 * the following format. - the size (as an Big Indian int) of the charsetName encoding in ASCII<br/>
	 * - the bytes encoding this charsetName in ASCII<br/>
	 * - the bytes encoding msg in charsetName.<br/>
	 * The returned ByteBuffer is in <strong>write mode</strong> (need to be flipped before to be used).
	 * 
	 * @param msg
	 *            the String to send
	 * @param charsetName
	 *            name of the Charset to encode the String msg
	 * @return a newly allocated ByteBuffer containing the representation of msg
	 */
	public static ByteBuffer encodeMessage(String msg, String charsetName) {
		Objects.requireNonNull(msg);
		Objects.requireNonNull(charsetName);
		ByteBuffer msgBuffer = Charset.forName(charsetName).encode(msg);
		ByteBuffer res = ByteBuffer.allocateDirect(Integer.BYTES + charsetName.length() + msgBuffer.limit());
		res.putInt(charsetName.length()).put(ASCII_CHARSET.encode(charsetName)).put(msgBuffer);
		return res;
	}

	public static void usage() {
		System.out.println("Usage : ClientBetterUpperCaseUDP host port charsetName");
	}

	public static void main(String[] args) throws IOException {
		// check and retrieve parameters
		if (args.length != 3) {
			usage();
			return;
		}
		String host = args[0];
		int port = Integer.valueOf(args[1]);
		String charsetName = args[2];

		SocketAddress dest = new InetSocketAddress(host, port);
		// buff to receive messages
		ByteBuffer buff = ByteBuffer.allocateDirect(BUFFER_SIZE);

		try (Scanner scan = new Scanner(System.in); DatagramChannel dc = DatagramChannel.open()) {
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				ByteBuffer packet = encodeMessage(line, charsetName);
				packet.flip();
				dc.send(packet, dest);
				buff.clear();
				dc.receive(buff);
				Optional<String> res = decodeMessage(buff);
				if (res.isPresent()) {
					System.out.println("Received: " + res.get());
				} else {
					System.out.println("Received an invalid paquet");
				}

			}
		}
	}

}
