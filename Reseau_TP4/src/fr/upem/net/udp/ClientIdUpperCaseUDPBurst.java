package fr.upem.net.udp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientIdUpperCaseUDPBurst {

	private static Logger logger = Logger.getLogger(ClientIdUpperCaseUDPBurst.class.getName());
	private static final Charset UTF8 = Charset.forName("UTF8");
	private static final int BUFFER_SIZE = 1024;
	private final List<String> lines;
	private final int nbLines;
	private final String[] upperCaseLines; //
	private final int timeout;
	private final String outFilename;
	private final InetSocketAddress serverAddress;
	private final DatagramChannel dc;
	private final BitSet received; // BitSet marking received requests

	private static void usage() {
		System.out.println("Usage : ClientIdUpperCaseUDPBurst in-filename out-filename timeout host port ");
	}

	private ClientIdUpperCaseUDPBurst(List<String> lines, int timeout, InetSocketAddress serverAddress,
		String outFilename) throws IOException {
		this.lines = lines;
		this.nbLines = lines.size();
		this.timeout = timeout;
		this.outFilename = outFilename;
		this.serverAddress = serverAddress;
		this.dc = DatagramChannel.open();
		dc.bind(null);
		this.received = new BitSet(nbLines);
		this.upperCaseLines = new String[nbLines];
	}
	
	private ByteBuffer[] initLinesBuffer() {
		ByteBuffer[] buffers = new ByteBuffer[lines.size()];
		for (int i = 0; i < lines.size(); i++) {
			ByteBuffer data = UTF8.encode(lines.get(i));
			buffers[i] = ByteBuffer.allocate(Long.BYTES + data.remaining());
		}
		return buffers;
	}

	private void senderThreadRun() {
		try {
			ByteBuffer[] linesBuffer = initLinesBuffer();
			for (;;) {
				for (int i = 0; i < lines.size(); i++) {
					boolean isResponseReceived;
					synchronized (received) {
						isResponseReceived = received.get(i);
					}
					if (!isResponseReceived) {
						dc.send(linesBuffer[i].flip(), serverAddress);
					}
				}
				Thread.sleep(timeout);
			}
		} catch (AsynchronousCloseException | InterruptedException e) {
			// normal behaviour
		} catch (IOException e) {
			logger.log(Level.SEVERE, "IOException", e);
		} finally {
			logger.info("Sender thread stopped");
		}
	}

	private void launch() throws IOException {
		Thread senderThread = new Thread(this::senderThreadRun);
		senderThread.start();
		ByteBuffer receiveData = ByteBuffer.allocateDirect(BUFFER_SIZE);
		int linesToReceive = lines.size();

		while (linesToReceive > 0) {
			receiveData.clear();
			dc.receive(receiveData);
			receiveData.flip();
			long id = receiveData.getLong();
			String msg = UTF8.decode(receiveData).toString();
			synchronized (received) {
				if (id >= 0 && id < lines.size() && !received.get((int) id)) {
					received.set((int) id);
					upperCaseLines[(int) id] = msg;
					linesToReceive--;
				}
			}
		}
		
		senderThread.interrupt();

		Files.write(Paths.get(outFilename), Arrays.asList(upperCaseLines), UTF8, StandardOpenOption.CREATE,
			StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length != 5) {
			usage();
			return;
		}

		String inFilename = args[0];
		String outFilename = args[1];
		int timeout = Integer.valueOf(args[2]);
		String host = args[3];
		int port = Integer.valueOf(args[4]);
		InetSocketAddress serverAddress = new InetSocketAddress(host, port);

		// Read all lines of inFilename opened in UTF-8
		List<String> lines = Files.readAllLines(Paths.get(inFilename), UTF8);
		// Create client with the parameters and launch it
		ClientIdUpperCaseUDPBurst client = new ClientIdUpperCaseUDPBurst(lines, timeout, serverAddress, outFilename);
		client.launch();

	}
}
