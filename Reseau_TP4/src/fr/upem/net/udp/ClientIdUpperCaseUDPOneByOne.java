package fr.upem.net.udp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientIdUpperCaseUDPOneByOne {

	private static Logger logger = Logger.getLogger(ClientIdUpperCaseUDPOneByOne.class.getName());
	private static final Charset UTF8 = Charset.forName("UTF8");
	private static final int BUFFER_SIZE = 1024;
	private final List<String> lines;
	private final List<String> upperCaseLines = new ArrayList<>(); //
	private final int timeout;
	private final String outFilename;
	private final InetSocketAddress serverAddress;
	private final DatagramChannel dc;

	private final BlockingQueue<Response> queue = new SynchronousQueue<>();

	private static void usage() {
		System.out.println("Usage : ClientIdUpperCaseUDPOneByOne in-filename out-filename timeout host port ");
	}

	private ClientIdUpperCaseUDPOneByOne(List<String> lines, int timeout, InetSocketAddress serverAddress,
		String outFilename) throws IOException {
		this.lines = lines;
		this.timeout = timeout;
		this.outFilename = outFilename;
		this.serverAddress = serverAddress;
		this.dc = DatagramChannel.open();
		dc.bind(null);
	}

	private void listenerThreadRun() {
		try {
			ByteBuffer receiveData = ByteBuffer.allocateDirect(BUFFER_SIZE);
			while (!Thread.interrupted()) {
				receiveData.clear();
				dc.receive(receiveData);
				receiveData.flip();
				long id = receiveData.getLong();
				String msg = UTF8.decode(receiveData).toString();
				Response r = new Response(id, msg);
				queue.put(r);
			}
		} catch (InterruptedException | AsynchronousCloseException e) {
			// normal behaviour
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length != 5) {
			usage();
			return;
		}

		String inFilename = args[0];
		String outFilename = args[1];
		int timeout = Integer.valueOf(args[2]);
		String host = args[3];
		int port = Integer.valueOf(args[4]);
		InetSocketAddress serverAddress = new InetSocketAddress(host, port);

		// Read all lines of inFilename opened in UTF-8
		List<String> lines = Files.readAllLines(Paths.get(inFilename), UTF8);
		// Create client with the parameters and launch it
		ClientIdUpperCaseUDPOneByOne client = new ClientIdUpperCaseUDPOneByOne(lines, timeout, serverAddress,
			outFilename);
		client.launch();

	}

	private void launch() throws IOException, InterruptedException {
		Thread listenerThread = new Thread(this::listenerThreadRun);
		listenerThread.start();
		long id = 0;

		for (String line : lines) {
			boolean isResponseReceive = false;
			ByteBuffer data = UTF8.encode(line);
			ByteBuffer request = ByteBuffer.allocateDirect(Long.BYTES + data.remaining());
			request.putLong(id).put(data);
			request.flip();

			while (!isResponseReceive) {
				dc.send(request, serverAddress);
				long start = System.currentTimeMillis();
				long currentTimeout = 0;
				while ((currentTimeout = System.currentTimeMillis() - start) < timeout) {
					// !!! timeout - currentTimeout ne doit jamais etre égal à 0 sinon poll attend indéfiniment
					Response response = queue.poll(timeout - currentTimeout, TimeUnit.MILLISECONDS);
					if (response == null) {
						break;
					} else {
						if (response.id != id) {
							logger.log(Level.INFO, "Le message reçu n'est pas le bon. Renvoie du message: " + line);
						} else {
							upperCaseLines.add(response.msg);
							isResponseReceive = true;
							id++;
							break;
						}
					}
				}
				request.flip();
			}
		}

		listenerThread.interrupt();

		Files.write(Paths.get(outFilename), upperCaseLines, UTF8, StandardOpenOption.CREATE, StandardOpenOption.WRITE,
			StandardOpenOption.TRUNCATE_EXISTING);
	}

	private static class Response {

		final long id;
		final String msg;

		Response(long id, String msg) {
			this.id = id;
			this.msg = msg;
		}

		public String toString() {
			return id + ": " + msg;
		}
	}
}
