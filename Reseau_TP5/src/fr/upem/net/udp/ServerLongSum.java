package fr.upem.net.udp;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.BitSet;
import java.util.HashMap;
import java.util.logging.Logger;

public class ServerLongSum {

	private static final Logger logger = Logger.getLogger(ServerLongSum.class.getName());
	private static final int OPERATION_CODE_BYTES = Byte.BYTES;

	// Operations code
	private static final byte OP_CODE = 1;
	private static final byte ACK_CODE = 2;
	private static final byte RES_CODE = 3;

	// Buffers size
	private static final int REQUEST_LENGTH = 4 * Long.BYTES + OPERATION_CODE_BYTES;
	private static final int ACK_BYTES = OPERATION_CODE_BYTES + 2 * Long.BYTES;
	private static final int RES_BYTES = OPERATION_CODE_BYTES + 2 * Long.BYTES;

	private final DatagramChannel dc;
	private final HashMap<InetSocketAddress, HashMap<Long, SumData>> clients = new HashMap<>();

	private final ByteBuffer ackBuffer = ByteBuffer.allocate(ACK_BYTES);
	private final ByteBuffer resBuffer = ByteBuffer.allocate(RES_BYTES);

	public ServerLongSum(int port) throws IOException {
		dc = DatagramChannel.open();
		dc.bind(new InetSocketAddress(port));
		logger.info("ServerLongSum started on port " + port);
	}

	public void serve() throws IOException {
		ByteBuffer receivedData = ByteBuffer.allocate(REQUEST_LENGTH);
		for (;;) {
			receivedData.clear();
			InetSocketAddress client = (InetSocketAddress) dc.receive(receivedData);
			receivedData.flip();
			if (!hasRequestValidSize(receivedData)) {
				continue; // Bad request, we ignore it
			}
			switch (receivedData.get()) {
			case OP_CODE:
				sumRequestTreatment(receivedData, client);
				break;
			}
		}
	}

	private boolean hasRequestValidSize(ByteBuffer bb) {
		return bb.remaining() >= REQUEST_LENGTH;
	}

	private void sumRequestTreatment(ByteBuffer request, InetSocketAddress client) throws IOException {
		long sessionId = request.getLong();
		int idPosOper = (int) request.getLong();
		int totalOper = (int) request.getLong();
		long opValue = request.getLong();

		HashMap<Long, SumData> clientData = clients.computeIfAbsent(client, k -> new HashMap<>());
		SumData sumData = clientData.computeIfAbsent(sessionId, k -> new SumData(totalOper));

		sumData.add(opValue, idPosOper);
		sendAck(sessionId, idPosOper, client);
		sendResultIfComplete(sessionId, sumData, client);
	}

	private void sendResultIfComplete(long sessionId, SumData sumData, InetSocketAddress client) throws IOException {
		if (sumData.isComplete()) {
			resBuffer.clear();
			resBuffer.put(RES_CODE).putLong(sessionId).putLong(sumData.sum);
			resBuffer.flip();
			dc.send(resBuffer, client);
		}
	}

	private void sendAck(long sessionId, long idPosOper, InetSocketAddress client) throws IOException {
		ackBuffer.clear();
		ackBuffer.put(ACK_CODE).putLong(sessionId).putLong(idPosOper);
		ackBuffer.flip();
		dc.send(ackBuffer, client);
	}

	public static void usage() {
		System.out.println("Usage : ServerIdUpperCaseUDP port");
	}
	
	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			usage();
			return;
		}
		ServerLongSum server;
		int port = Integer.valueOf(args[0]);
		if (!(port >= 1024) & port <= 65535) {
			logger.severe("The port number must be between 1024 and 65535");
			return;
		}
		try {
			server = new ServerLongSum(port);
		} catch (BindException e) {
			logger.severe("Server could not bind on " + port + "\nAnother server is probably running on this port.");
			return;
		}
		server.serve();
		//new Thread(server::cleanerThread).start();
	}

	private class SumData {

		final BitSet received;
		final int nbOperand;
		long sum = 0;
		int operandToReceive;

		SumData(int nbOperand) {
			received = new BitSet(nbOperand);
			this.nbOperand = nbOperand;
			this.operandToReceive = nbOperand;
		}

		boolean add(long value, int index) {
			if (index < 0 || index >= nbOperand) {
				logger.info("nbOp: " + nbOperand + "   index error: " + index);
				return false; // bad index
			}
				if (!received.get(index)) {
					received.set(index);
					sum += value;
					operandToReceive--;
					return true;
				}
			
			return false;
		}

		boolean isComplete() {
				return operandToReceive == 0;
		}
	}

}
