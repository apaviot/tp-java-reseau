package fr.upem.net.udp;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.util.logging.Logger;

public class ServerIdUpperCaseUDP {

	private static final Logger logger = Logger.getLogger(ServerIdUpperCaseUDP.class.getName());
	private static final int BUFFER_SIZE = 1024;
	private static final Charset UTF8 = Charset.forName("UTF8");
	private final DatagramChannel dc;

	public ServerIdUpperCaseUDP(int port) throws IOException {
		dc = DatagramChannel.open();
		dc.bind(new InetSocketAddress(port));
		logger.info("ServerBetterUpperCaseUDP started on port " + port);
	}

	public void serve() throws IOException {
		ByteBuffer receivedData = ByteBuffer.allocate(BUFFER_SIZE);
		while (!Thread.interrupted()) {
			receivedData.clear();
			SocketAddress client = dc.receive(receivedData);
			receivedData.flip();
			if (receivedData.remaining() < Long.BYTES) {
				continue;
			}
			long id = receivedData.getLong();
			String msg = UTF8.decode(receivedData).toString();
			
			ByteBuffer upperMsg = UTF8.encode(msg.toUpperCase());
			ByteBuffer response = ByteBuffer.allocate(Long.BYTES + upperMsg.remaining());
			response.putLong(id).put(upperMsg);
			response.flip();
			dc.send(response, client);
		}
	}

	public static void usage() {
		System.out.println("Usage : ServerIdUpperCaseUDP port");
	}

	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			usage();
			return;
		}
		ServerIdUpperCaseUDP server;
		int port = Integer.valueOf(args[0]);
		if (!(port >= 1024) & port <= 65535) {
			logger.severe("The port number must be between 1024 and 65535");
			return;
		}
		try {
			server = new ServerIdUpperCaseUDP(port);
		} catch (BindException e) {
			logger.severe("Server could not bind on " + port + "\nAnother server is probably running on this port.");
			return;
		}
		server.serve();
	}
}