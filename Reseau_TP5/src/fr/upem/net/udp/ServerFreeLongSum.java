package fr.upem.net.udp;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class ServerFreeLongSum {

	private static final Logger logger = Logger.getLogger(ServerFreeLongSum.class.getName());
	private static final int OPERATION_CODE_BYTES = Byte.BYTES;
	private static final long SESSION_TIMEOUT = 10_000L;

	// Operations code
	private static final byte OP_CODE = 1;
	private static final byte ACK_CODE = 2;
	private static final byte RES_CODE = 3;
	private static final byte CLEAR_CODE = 4;
	private static final byte CLEAN_ACK_CODE = 5;

	// Buffers size
	private static final int REQUEST_LENGTH = 4 * Long.BYTES + OPERATION_CODE_BYTES;
	private static final int ACK_BYTES = OPERATION_CODE_BYTES + 2 * Long.BYTES;
	private static final int RES_BYTES = OPERATION_CODE_BYTES + 2 * Long.BYTES;

	private final DatagramChannel dc;
	private final HashMap<InetSocketAddress, HashMap<Long, SumData>> clients = new HashMap<>();

	private final ByteBuffer ackBuffer = ByteBuffer.allocate(ACK_BYTES);
	private final ByteBuffer resBuffer = ByteBuffer.allocate(RES_BYTES);
	private final ByteBuffer cleanBuffer = ByteBuffer.allocate(OPERATION_CODE_BYTES + Long.BYTES);

	public ServerFreeLongSum(int port) throws IOException {
		dc = DatagramChannel.open();
		dc.bind(new InetSocketAddress(port));
		logger.info("ServerLongSum started on port " + port);
	}

	public void serve() throws IOException {
		ByteBuffer receivedData = ByteBuffer.allocate(REQUEST_LENGTH);
		for (;;) {
			receivedData.clear();
			InetSocketAddress client = (InetSocketAddress) dc.receive(receivedData);
			receivedData.flip();
			/*
			if (!hasRequestValidSize(receivedData)) {
				continue; // Bad request, we ignore it
			}
			*/
			byte code = receivedData.get();
			//System.out.println(code);
			switch (code) {
			case OP_CODE:
				sumRequestTreatment(receivedData, client);
				break;
			case CLEAR_CODE:
				logger.info("CLEAN_ASK");
				cleanTreatment(receivedData, client);
				break;
			}
		}
	}

	private boolean hasRequestValidSize(ByteBuffer bb) {
		return bb.remaining() >= REQUEST_LENGTH;
	}

	private void cleanTreatment(ByteBuffer request, InetSocketAddress client) throws IOException {
		long sessionId = request.getLong();
		clients.getOrDefault(client, new HashMap<>()).remove(sessionId);
		//logger.info("Session n°" + sessionId + " clean");
		sendCleanAck(sessionId, client);
	}

	private void sendCleanAck(long sessionId, InetSocketAddress client) throws IOException {
		cleanBuffer.clear();
		cleanBuffer.put(CLEAN_ACK_CODE).putLong(sessionId);
		cleanBuffer.flip();
		dc.send(cleanBuffer, client);
	}

	private void sumRequestTreatment(ByteBuffer request, InetSocketAddress client) throws IOException {
		long sessionId = request.getLong();
		int idPosOper = (int) request.getLong();
		int totalOper = (int) request.getLong();
		long opValue = request.getLong();

		HashMap<Long, SumData> clientData = clients.computeIfAbsent(client, k -> new HashMap<>());
		SumData sumData = clientData.computeIfAbsent(sessionId, k -> new SumData(totalOper));

		sumData.add(opValue, idPosOper);
		sendAck(sessionId, idPosOper, client);
		sendResultIfComplete(sessionId, sumData, client);
	}

	private void sendResultIfComplete(long sessionId, SumData sumData, InetSocketAddress client) throws IOException {
		if (sumData.isComplete()) {
			resBuffer.clear();
			resBuffer.put(RES_CODE).putLong(sessionId).putLong(sumData.sum);
			resBuffer.flip();
			dc.send(resBuffer, client);
		}
	}

	private void sendAck(long sessionId, long idPosOper, InetSocketAddress client) throws IOException {
		ackBuffer.clear();
		ackBuffer.put(ACK_CODE).putLong(sessionId).putLong(idPosOper);
		ackBuffer.flip();
		dc.send(ackBuffer, client);
	}

	public static void usage() {
		System.out.println("Usage : ServerIdUpperCaseUDP port");
	}

	public void cleanerThread() {
		System.out.println("CLEANER");
		while (!Thread.interrupted()) {
			clients.forEach((k, v) -> {
				Set<Long> ids = new HashSet<>(v.keySet());
				for (long id : ids) {
					if (v.get(id).isExpired()) {
						logger.info("Session n°" + v.get(id) + " clean");
						v.remove(id);
					}
				}
			});
		}
	}

	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			usage();
			return;
		}
		ServerFreeLongSum server;
		int port = Integer.valueOf(args[0]);
		if (!(port >= 1024) & port <= 65535) {
			logger.severe("The port number must be between 1024 and 65535");
			return;
		}
		try {
			server = new ServerFreeLongSum(port);
			new Thread(server::cleanerThread).start();
		} catch (BindException e) {
			logger.severe("Server could not bind on " + port + "\nAnother server is probably running on this port.");
			return;
		}
		server.serve();
		
	}

	private class SumData {

		final BitSet received;
		final int nbOperand;
		long sum = 0;
		int operandToReceive;
		long lastMeet;

		SumData(int nbOperand) {
			received = new BitSet(nbOperand);
			this.nbOperand = nbOperand;
			this.operandToReceive = nbOperand;
			this.lastMeet = System.currentTimeMillis();
		}

		boolean add(long value, int index) {
			synchronized (received) {
				this.lastMeet = System.currentTimeMillis();
			}
			if (index < 0 || index >= nbOperand) {
				logger.info("nbOp: " + nbOperand + "   index error: " + index);
				return false; // bad index
			}
			synchronized (received) {
				if (!received.get(index)) {
					received.set(index);
					sum += value;
					operandToReceive--;
					return true;
				}
			}
			return false;
		}

		boolean isComplete() {
			synchronized (received) {
				this.lastMeet = System.currentTimeMillis();
				return operandToReceive == 0;
			}
		}

		boolean isExpired() {
			return System.currentTimeMillis() - lastMeet >= SESSION_TIMEOUT;
		}
	}

}
